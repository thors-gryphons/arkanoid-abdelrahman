
using UnityEngine;

public class BallPhysics : MonoBehaviour
{
    public float speed = 55;
    Vector2 originalPosition;
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
        originalPosition = transform.position;
    }

    float hitfactor(Vector2 ballPos, Vector2 racketPos, float racketWidth)
    {
        return (ballPos.x - racketPos.x) / racketWidth;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "Racket")
        {
            float x = hitfactor(transform.position, col.transform.position, col.collider.bounds.size.x);
            Vector2 dir = new Vector2(x, 1).normalized;
            GetComponent<Rigidbody2D>().velocity = dir * speed;

        }
        if (col.gameObject.name == "LowerBorder")
        {
            transform.position = originalPosition;
            GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
        }
    }
}
