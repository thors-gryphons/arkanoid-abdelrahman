
using UnityEngine;

public class BreakBlock : MonoBehaviour
{
    public bool isStrongBlock = false;
    public int blockHP = 2;
    void OnCollisionEnter2D(Collision2D collisioninfo)
    {
        if (isStrongBlock) blockHP--;
        else blockHP = 0;
        
        if (blockHP <= 0) Destroy(gameObject);
    }
}
